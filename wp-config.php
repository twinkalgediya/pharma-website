<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pharma');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9:f7DEEo J6-Lzg&Ycsiui!sw@s}!+Uj+-RTF<nb9C:yYapXjc BHmSD<a;/nGG:');
define('SECURE_AUTH_KEY',  ': NY/C+o~#ep,m(xs$/Vd*&$x20>3HaC0BSW{b&X;{m}cgeKa,xEwu?DH: Te$uk');
define('LOGGED_IN_KEY',    'rZQj)mq.-AP)ez%Qxoxx5{{. 3T}[}49p)`^{({.?$n:9W@eIaQdB]p1w90*W*r!');
define('NONCE_KEY',        'Ms3-1m&p DIS!cXE[7lwwVqg/y~G3/8r;{&!!v,Aj|}V[Q7+caORfM8yc[/i.ck&');
define('AUTH_SALT',        'rDt?H{*vi4g1zrCS/ySR1XPT`kCZ@U$ZdBOx _Nn4=q(;8=:(<yZZ``lY7b<6U@E');
define('SECURE_AUTH_SALT', 'm:~YIJ.E{/T+;0?L0:lQ,5VBkHAZn)~x7ga<*rD0O9Ru^C#Ke+YR; {wHQ_/hWt&');
define('LOGGED_IN_SALT',   '}!rKa 4Y8hwbjS#OVLgN,)_~pLzngb+[(SdgMhJNFFE$In& -f=;>2<m>P,qH+1w');
define('NONCE_SALT',       'idi!$7*2mG%7w|XiHQ.V/?)4txm0z}Q~+Jj%&*bos#t#k;-2 QJ]twx%[0~+v|m@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
